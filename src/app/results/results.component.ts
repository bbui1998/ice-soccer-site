import { Component, OnInit } from '@angular/core';
import {Result} from '../result';
import {DataService} from '../data.service'
@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.css']
})
export class ResultsComponent implements OnInit {

  results:Result[];

  constructor(private dataService:DataService) { }

  ngOnInit() {
    this.loadTableR();
  }
loadTableR(){
  this.dataService.getTableR('450').subscribe(result=>{this.results = result;
  console.log(this.results);}
  );
}