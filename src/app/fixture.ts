import {Result} from './result';
import{Home} from './home';
import{Away} from './away';

export class Fixture {
    constructor( 
        public _links: string,
        public id: string,
    public competitionId: string,
    public fixtures: string,
    public count: string,
    public date: string,
    public matchday: string,
    public homeTeamName: string,
    public homeTeamId: string,
    public awayTeamName: string,
    public awayTeamId: String,
    public timeFrameStart: string,
    public timeFrameEnd: string,
    public home: Home,
    public away: Away)
{}
}
